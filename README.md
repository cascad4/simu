# simu


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/cascad4/simu.git
git branch -M main
git push -uf origin main
```

# Usage

## Gcode

Code | Description
:---: | :---
G0 | Bascule en mode simulation
G1 | Bascule en mode manuel
G2 | Bascule en mode édition
M <macro: u32> | Appelle la macro 'macro'

### Mode simulation

Code | Description
:---: | :---
T <dt: f32> | Change à 'dt' ms l'intervalle de temps entre 2 pas
N <n: u32> | Simule les n suivants pas

### Mode manuel

Code | Description
:---: | :---
S <id: u32> | Sélectionne l'objet identifié par 'id'
X <x: f32> | Modifie à 'x' l'abscisse de l'objet selectionné
Y <y: f32> | Modifie à 'y' l'ordonnée de l'objet selectionné
A <a: f32> | Modifie à 'a' l'angle de l'objet selectionné
U <u: f32> | Modifie à 'u' la vitesse de l'objet selectionné selon x
V <v: f32> | Modifie à 'v' la vitesse de l'objet selectionné selon y
W <w: f32> | Modifie à 'a' la vitesse angulaire de l'objet selectionné

### Mode édition

Code | Description
:---: | :---
N <id: u32> | Crée et sélectionne l'objet identifié par 'id'
S <id: u32> | Sélectionne l'objet identifié par 'id'
D <id: u32> | Supprime l'objet identifié par 'id'
C <a0: f32> Vec<C: f32, phi: f32> | Défini le contour¹ de l'objet sélectionné
T <type: u32> | Défini le type de l'objet
P <prop: u32, val: f32> | Met la propriété 'prop' à 'val'
F0 | Crée une forme point (pas de collisions) et la sélectionne
F1 | Crée une forme ligne (pas de collisions) et la sélectionne
F2 | Crée une forme cercle et la sélectionne
F3 | Crée une forme rectangle et la sélectionne
F4 | Crée une forme image et la sélectionne
X <x: f32> | Modifie à 'x' l'abscisse de la forme sélectionnée
Y <y: f32> | Modifie à 'y' l'ordonnée de la forme sélectionnée
H <h: f32> | Modifie à 'h' la hauteur de la forme sélectionnée (pour rectangle, image et ligne)
W <w: f32> | Modifie à 'w' la largeur de la forme sélectionnée (pour rectangle, image et ligne)
A <a: f32> | Modifie à 'a' l'angle de la forme sélectionnée (pour rectangle et image)
R <r: f32> | Modifie à 'r' le rayon de la forme sélectionnée (pour cercle)
I <i: u32> | Modifie à 'i' l'image de la forme sélectionnée (pour image)

[¹] Si M un point sur le contour de l'objet, M(t) = (f(t)×cos(t), f(t)×sin(t)) avec :  
f(t) = a0 + C[1]×cos(1×t+phi[1]) + ... + C[n]×cos(n×t + phi[n])

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
