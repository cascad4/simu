use crate::{math::{coordonnees::{Position, Vitesse, Scalaire, Vecteur, Point, Acceleration}, intersection::{Collision, Intersection}}, forme::Forme};

#[derive(Debug, Clone)]
pub struct Objet {
    pub id: u32,

    pub mobile: bool,
    pub solide: bool,

    pub masse: f32,
    pub inertie: f32,
    pub materiau: u32,
    
    pub position: Position,
    pub vitesse: Vitesse,

    pub forme: Forme,
}

impl Objet {
    pub fn collision(&self, obj: &Objet, pas: Scalaire) -> Option<Collision> {
        Collision::intersecter(
            &self.forme,
            &obj.forme,
            self.position,
            obj.position,
            self.vitesse,
            obj.vitesse,
            pas,
        ) 
    }

    pub fn vitesse_selon(&self, direction: Vecteur) -> Scalaire {
        self.vitesse.origine().produit_scalaire(direction)
    }

    pub fn vitesse_point(&self, point: Point) -> Vecteur {
        self.vitesse.origine() + self.vitesse.a * Vecteur::orthogonal(point)
    }

    pub fn deplacer(&mut self, acc: Acceleration, dt: Scalaire) {
        self.vitesse += acc * dt;
        self.position += self.vitesse * dt;
    }

    pub fn imposer_vitesse_point(&mut self, v: Vecteur, point: Point) {
        //let vi = self.vitesse_point(point);
        //self.vitesse.x += vitesse.x - vi.x;
        //self.vitesse.y += vitesse.y - vi.y;
        //self.vitesse.a = Vecteur::orthonorme(point).produit_scalaire(vi);
        self.vitesse = Vitesse {
            x: v.x,
            y: v.y,
            a: - v.produit_scalaire(Vecteur::orthonorme(point)),
        }
    }
}
