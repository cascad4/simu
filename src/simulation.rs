use crate::{objet::Objet, math::{coordonnees::{Vecteur, Acceleration, Scalaire}, intersection::{Collision, Intersection}}, force::Force};

type TableauCollisions = Vec<Vec<Option<Collision>>>;

#[derive(Debug, Clone)]
pub struct Simulation {
    pub objets: Vec<Objet>,
    pub dt: f32,
    pub t: f32,
    forces: Vec<Vec<Force>>,
    collisions: Vec<Vec<bool>>,
}

impl Simulation {
    pub fn nouveau(pas: Scalaire) -> Simulation {
        Simulation {
            objets: vec![],
            dt: pas,
            t: 0.,
            forces: vec![],
            collisions: vec![],
        }
    }

    pub fn ajouter(&mut self, objet: Objet) {
        let l = self.objets.len();
        self.objets.push(objet);
        self.objets[l].id = l as u32;
        
        self.forces.push(vec![]);
        
        self.collisions.push(vec![]);
        for i in 0..self.collisions.len() {
            self.collisions[i].push(false);
        }
    }

    fn en_collision(&self, a: usize, b: usize) -> bool {
        if a >= b {
            self.collisions[a][b]
        } else {
            self.collisions[b][a]
        }
    }
    fn marquer_collision(&mut self, a: usize, b: usize, etat: bool) {
        if a >= b {
            self.collisions[a][b] = etat;
        } else {
            self.collisions[b][a] = etat;
        }
    }

    pub fn pas(&mut self) {
        let tableau = self.tableau_collisions();

        for i in 0..self.objets.len() {
            if self.objets[i].mobile {
                self.bilan_forces(i, &self.collisions(i, &tableau));
            }
        }

        //dbg!(&self);
        for i in 0..self.objets.len() {
            if self.objets[i].mobile {
                self.pfd(i);
            }
        }
        
        self.t += self.dt;
    }

    fn tableau_collisions(&mut self) -> TableauCollisions {
        let mut collisions = vec![];

        for i in 0..self.objets.len() {
            collisions.push(vec![]);
            for j in 0..i {
                collisions[i].push(
                    match self.en_collision(i, j) {
                        false => self.objets[i].collision(&self.objets[j], self.dt),
                        true => None,
                    }
                );
            }
        }

        collisions
    }

    fn collisions(&self, obj: usize, collisions: &TableauCollisions) -> Vec<Option<Collision>> {
        let mut col = vec![];
        
        for i in 0..self.objets.len() {
            if i > obj {
                col.push(match &collisions[i][obj] {
                    Some(collision) => Some(collision.inverse()),
                    None => None,
                });
            } else if i < obj {
                col.push(collisions[obj][i].clone());
            } else {
                col.push(None);
            }
        }

        col
    }

    fn bilan_forces(&mut self, obj: usize, collisions: &Vec<Option<Collision>>) {
        self.forces[obj].push(Force::poids(self.objets[obj].masse, self.t, self.dt));
        self.forces[obj].push(Force::trainee(0.01, self.objets[obj].vitesse, self.t, self.dt));
        
        for i in 0..collisions.len() {
            match &collisions[i] {
                Some(col) => {
                    if col.temps >= 0. && col.temps <= self.dt {
                        self.forces[obj].push(Force::collision(&col, &self.objets[obj], &self.objets[i], self.t, self.dt));
                        self.marquer_collision(obj, i, true);
                    }
                },
                None => (),
            }
        }
    }

    fn pfd(&mut self, obj: usize) {
        let mut croissant = vec![];
        let mut actives = vec![];

        for i in 0..self.forces[obj].len() {
            let mut min = i;
            for j in i..self.forces[obj].len() {
                if self.forces[obj][j].debut < self.forces[obj][min].debut {
                    min = j;
                }
            }
            croissant.push(min);
        }

        let m = self.objets[obj].masse;
        let j = self.objets[obj].inertie;

        //dbg!(self.t);
        let mut t = self.t;
        while t >= 0. && t <= self.t + self.dt + Scalaire::INFINITY {
            let mut resultante = Vecteur::nouveau(0., 0.);
            let mut moment = 0.;
            for i in 0..actives.len() {
                resultante += self.forces[obj][i].resultante();
                moment += self.forces[obj][i].moment();
            }
            let a = Acceleration {
                x: resultante.x / m,
                y: resultante.y / m,
                a: moment / j,
            };
            
            let mut source = 0;
            let mut dt = self.t + self.dt - t;
            match croissant.get(0) {
                Some(i) => {
                    let d = self.forces[obj][*i].debut - self.t;
                    if d < dt {
                        dt = d.max(0.);
                        source = 1;
                    }
                },
                None => (),
            }
            match actives.get(0) {
                Some(i) => {
                    let d = self.forces[obj][*i as usize].fin - self.t;
                    if d < dt {
                        dt = d.max(0.);
                        source = 2;
                    }
                },
                None => (),
            }
            //dbg!(t, dt, source);
            let v = self.objets[obj].vitesse;
            self.objets[obj].position += v * dt;
            self.objets[obj].vitesse +=  a * dt;
            t += dt;
            if croissant.len() > 0 && source == 1 {
                let mut i = actives.len();
                actives.push(croissant.remove(0));
                while i > 0 && self.forces[obj][actives[i]].fin < self.forces[obj][actives[i - 1]].fin {
                    (actives[i-1], actives[i]) = (actives[i], actives[i-1]);
                    i -= 1;
                }
            } else if actives.len() > 0 && source == 2 {
                if t > self.forces[obj][actives[0]].fin {
                    actives.remove(0);
                } 
            } else if source == 0 {
                break;
            }
        }
        let mut i = 0;
        while i < self.forces[obj].len() {
            let f = &self.forces[obj][i];
            if f.fin < t {
                if f.source < self.objets.len() {
                    self.marquer_collision(obj, f.source, false)
                }
                self.forces[obj].remove(i);
            } else {
                i += 1;
            }
        }
    }
}
