use crate::math::coordonnees::Scalaire;

// 0: Plastique

pub static F: [[Scalaire; 1]; 1] = [
    [0.1],
];

pub static E: [[Scalaire; 1]; 1] = [
    [0.3],
];
