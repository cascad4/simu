use std::{ops::{Add, Neg, Sub, Mul, Div, AddAssign}, fmt::Display, f32::consts::PI};

pub type Scalaire = f32;

pub type Position = Repere;
pub type Vitesse = Repere;
pub type Acceleration = Repere;

pub type Point = Vecteur; 

#[derive(Debug, Clone, Copy)]
pub struct Repere {
    pub x: Scalaire,
    pub y: Scalaire,
    pub a: Scalaire,
}

impl Repere {
    pub const NUL: Repere = Repere { x: 0., y: 0., a: 0. };

    pub fn nouveau(x: Scalaire, y: Scalaire, a: Scalaire) -> Repere {
        Repere {
            x,
            y,
            a,
        }
    }

    pub fn produit_scalaire(&self, autre: Repere) -> Scalaire {
        self.x * autre.x + self.y * autre.y
    }

    pub fn norme(&self) -> Scalaire {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn distance(self, autre: Repere) -> Scalaire {
        (autre - self).norme()
    }

    pub fn origine(&self) -> Vecteur {
        Vecteur {
            x: self.x,
            y: self.y,
        }
    }
}

impl Display for Repere {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {}, {}).T", self.x, self.y, self.a)
    }
}

impl Add<Repere> for Repere {
    type Output = Repere;

    fn add(self, autre: Repere) -> Self::Output {
        Repere {
            x: self.x + autre.x,
            y: self.y + autre.y,
            a: self.a + autre.a,
        }
    }
}

impl Neg for Repere {
    type Output = Repere;

    fn neg(self) -> Self::Output { 
        Repere {
            x: - self.x,
            y: - self.y,
            a: - self.a,
        }
    }
}

impl Sub<Repere> for Repere {
    type Output = Repere;

    fn sub(self, autre: Repere) -> Self::Output {
        Repere {
            x: self.x - autre.x,
            y: self.y - autre.y,
            a: self.a - autre.a,
        }
    }
}

impl Mul<Scalaire> for Repere {
    type Output = Repere;

    fn mul(self, autre: Scalaire) -> Self::Output {
        Repere {
            x: self.x * autre,
            y: self.y * autre,
            a: self.a * autre,
        }
    }
}

impl Mul<Repere> for Scalaire {
    type Output = Repere;

    fn mul(self, autre: Repere) -> Self::Output {
        Repere {
            x: self * autre.x,
            y: self * autre.y,
            a: self * autre.a,
        }
    }
}

impl Div<Scalaire> for Repere {
    type Output = Repere;

    fn div(self, autre: Scalaire) -> Self::Output {
        Repere {
            x: self.x / autre,
            y: self.y / autre,
            a: self.a / autre,
        }
    }
}

impl AddAssign for Repere {
    fn add_assign(&mut self, autre: Self) {
        self.x += autre.x;
        self.y += autre.y;
        self.a += autre.a;
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Vecteur {
    pub x: Scalaire,
    pub y: Scalaire,
}

impl Vecteur {
    pub const NUL: Vecteur = Vecteur { x: 0., y: 0. };

    pub fn nouveau(x: Scalaire, y: Scalaire) -> Vecteur {
        Vecteur {
            x,
            y,
        }
    }

    pub fn orthogonal(autre: Vecteur) -> Vecteur {
        Vecteur { 
            x: - autre.y,
            y: autre.x,
        }
    }

    pub fn unitaire(autre: Vecteur) -> Vecteur {
        autre / autre.norme()
    }

    pub fn orthonorme(autre: Vecteur) -> Vecteur {
        Vecteur::orthogonal(Vecteur::unitaire(autre))
    }

    pub fn trigo(r: Scalaire, theta: Scalaire) -> Vecteur {
        Vecteur {
            x: r * theta.cos(),
            y: r * theta.sin(),
        }
    }

    pub fn produit_scalaire(&self, autre: Vecteur) -> Scalaire {
        self.x * autre.x + self.y * autre.y
    }

    pub fn norme(&self) -> Scalaire {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn argument(&self) -> Scalaire {
        if self.x > 0. {
            (self.y / self.x).atan()
        } else {
            (self.y / self.x).atan() + PI
        }
    }

    pub fn distance(self, autre: Vecteur) -> Scalaire {
        (autre - self).norme()
    }


    /// Si on  considère que les  coordonnées  du point
    /// `point` sont exprimées dans le repère `repere`,
    /// point.absolu(repere)  donne  les coordonnées de
    /// `point` par rapport à l'origine.
    pub fn projeter_hors(&self, repere: Repere) -> Vecteur {
        repere.origine() + Vecteur::nouveau(
            repere.a.cos() * self.x - repere.a.sin() * self.y,
            repere.a.sin() * self.x + repere.a.cos() * self.y,
        )
    }

    /// Projète le vecteur dans le repère donné
    pub fn projeter_dans(&self, repere: Repere) -> Vecteur {
        Vecteur::nouveau(
            repere.a.cos() * (self.x - repere.x) + repere.a.sin() * (self.y - repere.y),
            - repere.a.sin() * (self.x - repere.x) + repere.a.cos() * (self.y - repere.y),
        )
    }
}

impl Display for Vecteur {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Add<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn add(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self.x + autre.x,
            y: self.y + autre.y,
        }
    }
}

impl Neg for Vecteur {
    type Output = Vecteur;

    fn neg(self) -> Self::Output { 
        Vecteur {
            x: - self.x,
            y: - self.y,
        }
    }
}

impl Sub<Vecteur> for Vecteur {
    type Output = Vecteur;

    fn sub(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self.x - autre.x,
            y: self.y - autre.y,
        }
    }
}

impl Mul<Scalaire> for Vecteur {
    type Output = Vecteur;

    fn mul(self, autre: Scalaire) -> Self::Output {
        Vecteur {
            x: self.x * autre,
            y: self.y * autre,
        }
    }
}

impl Mul<Vecteur> for Scalaire {
    type Output = Vecteur;

    fn mul(self, autre: Vecteur) -> Self::Output {
        Vecteur {
            x: self * autre.x,
            y: self * autre.y,
        }
    }
}

impl Div<Scalaire> for Vecteur {
    type Output = Vecteur;

    fn div(self, autre: Scalaire) -> Self::Output {
        Vecteur {
            x: self.x / autre,
            y: self.y / autre,
        }
    }
}

impl AddAssign for Vecteur {
    fn add_assign(&mut self, autre: Self) {
        self.x += autre.x;
        self.y += autre.y;
    }
}
