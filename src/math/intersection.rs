use std::ops::{Add, Neg, Sub, Mul, Div};

use super::coordonnees::{Point, Scalaire, Vecteur, Repere, Vitesse, Position};

#[derive(Debug, Clone)]
pub struct Collision {
    pub point: Point, // Point de collision
    pub point1: Point, // Point de collision dans forme 1
    pub point2: Point, // Point de collision dans forme 2
    pub temps: Scalaire, // Distance avant la collision 
    pub normale: Vecteur, // Direction de la normale de la collision de 1 vers 2 (unitaire)
}

impl Collision {
    pub fn inverse(&self) -> Collision {
        Collision {
            point: self.point,
            point1: self.point2,
            point2: self.point1,
            temps: self.temps,
            normale: -self.normale
        }
    }

    pub const NUL: Collision = Collision {
        point: Point::NUL,
        point1: Point::NUL,
        point2: Point::NUL,
        temps: Scalaire::NEG_INFINITY,
        normale: Vecteur::NUL,
    };
}

impl Add<Collision> for Collision {
    type Output = Collision;

    fn add(self, autre: Collision) -> Self::Output {
        Collision {
            point: self.point + autre.point,
            point1: self.point1 + autre.point1,
            point2: self.point2 + autre.point2,
            temps: self.temps + autre.temps,
            normale: self.normale + autre.normale,
        }
    }
}

impl Neg for Collision {
    type Output = Collision;

    fn neg(self) -> Self::Output {
        Collision {
            point: - self.point,
            point1: - self.point1,
            point2: - self.point2,
            temps: - self.temps,
            normale: - self.normale,
        }
    }
}

impl Sub<Collision> for Collision {
    type Output = Collision;

    fn sub(self, autre: Collision) -> Self::Output {
        Collision {
            point: self.point - autre.point,
            point1: self.point1 - autre.point1,
            point2: self.point2 - autre.point2,
            temps: self.temps - autre.temps,
            normale: self.normale - autre.normale,
        }
    }
}

impl Mul<Scalaire> for Collision {
    type Output = Collision;

    fn mul(self, autre: Scalaire) -> Self::Output {
        Collision {
            point: self.point * autre,
            point1: self.point1 * autre,
            point2: self.point2 * autre,
            temps: self.temps * autre,
            normale: self.normale * autre,
        }
    }
}

impl Mul<Collision> for Scalaire {
    type Output = Collision;

    fn mul(self, autre: Collision) -> Self::Output {
        Collision {
            point: self * autre.point,
            point1: self * autre.point1,
            point2: self * autre.point2,
            temps: self * autre.temps,
            normale: self * autre.normale,
        }
    }
}

impl Div<Scalaire> for Collision {
    type Output = Collision;

    fn div(self, autre: Scalaire) -> Self::Output {
        Collision {
            point: self.point / autre,
            point1: self.point1 / autre,
            point2: self.point2 / autre,
            temps: self.temps / autre,
            normale: self.normale / autre,
        }
    }
}

const PALIERS: Scalaire = 32.;
pub trait Intersection<A, B> where Self: Sized {
    fn intersection(
        forme1: &A, forme2: &B,
        position1: Position, position2: Position,
    ) -> bool;

    fn collision(f1: &A, f2: &B, p1: Position, p2: Position,
        v1: Vitesse, v2: Vitesse, temps: Scalaire
    ) -> Self;
    
    fn intersecter(
        f1: &A, f2: &B,
        p1: Position, p2: Position,
        v1: Vitesse, v2: Vitesse, pas: Scalaire,
    ) -> Option<Self> {
        if Self::intersection(f1, f2, p1 - v1 * pas, p2 - v2 * pas) {
            return Some(Self::collision(f1, f2, p1, p2, v1, v2, -pas));
        }
        if !Self::intersection(f1, f2, p1 + v1 * pas, p2 + v2 * pas) {
            return None;
        }
        let (mut d, mut g) = (-pas, pas);
        while d - g > pas / PALIERS {
            let m = (g + d) / 2.;
            if Self::intersection(f1, f2, p1 + v1 * m, p2 + v2 * m) {
                d = m;
            } else {
                g = m;
            }
        }
        Some(Self::collision(f1, f2, p1, p2, v1, v2, d))
    }
}

#[derive(Debug, Clone)]
pub struct Cercle {
    pub centre: Point,
    pub rayon: Scalaire,
}

impl Cercle {
    pub fn nouveau(centre: Vecteur, rayon: Scalaire) -> Cercle {
        Cercle {
            centre,
            rayon,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Droite {
    pub debut: Point,
    pub fin: Point,
}

impl Droite {
    pub fn nouveau(debut: Point, fin: Point) -> Droite {
        Droite {
            debut,
            fin,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Segment {
    pub debut: Point,
    pub fin: Point,
}

impl Segment {
    pub fn nouveau(debut: Point, fin: Point) -> Segment {
        Segment {
            debut,
            fin,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Rectangle {
    pub ancre: Repere,
    pub dimensions: Vecteur,
}

impl Rectangle {
    pub fn nouveau(ancre: Repere, dimensions: Vecteur) -> Rectangle {
        Rectangle { 
            ancre,
            dimensions,
        }
    }
}

impl Intersection<Cercle, Cercle> for Collision {
    fn intersection(
            forme1: &Cercle, forme2: &Cercle,
            position1: Position, position2: Position,
        ) -> bool {
        let d = forme1.rayon + forme2.rayon;
        let vecteur = forme2.centre.projeter_hors(position2)
                    - forme1.centre.projeter_hors(position1);
        vecteur.norme() <= d
    }

    fn collision(f1: &Cercle, f2: &Cercle, p1: Position, p2: Position,
            v1: Vitesse, v2: Vitesse, temps: Scalaire
        ) -> Self {
        let p1 = p1 + temps * v1;
        let p2 = p2 + temps * v2;
        let a = f1.centre.projeter_hors(p1);
        let b = f2.centre.projeter_hors(p2);
        let p = a + Vecteur::unitaire(b - a) * f1.rayon;

        Collision {
            point: p,
            point1: p.projeter_dans(p1),
            point2: p.projeter_dans(p2),
            temps,
            normale: Vecteur::unitaire(a - b),
        }
    }
}

impl Intersection<Cercle, Droite> for Collision {
    fn intersection(
            forme1: &Cercle, forme2: &Droite,
            position1: Position, position2: Position,
        ) -> bool {
        let a = forme2.debut.projeter_hors(position2);
        let b = forme2.fin.projeter_hors(position2);
        let ab = Vecteur::unitaire(b - a);
        let ac = forme1.centre.projeter_hors(position1) - a;
        let d = (ac - ab * ac.produit_scalaire(ab)).norme();
        d <= forme1.rayon
    }

    fn collision(f1: &Cercle, f2: &Droite, p1: Position, p2: Position,
            v1: Vitesse, v2: Vitesse, temps: Scalaire
        ) -> Self {
        let p1 = p1 + temps * v1;
        let p2 = p2 + temps * v2;
        let a = f2.debut.projeter_hors(p2);
        let b = f2.fin.projeter_hors(p2);
        let c = f1.centre.projeter_hors(p1);
        let p = a + (b - a) * (b - a).produit_scalaire(c - a);

        let normale = Vecteur::unitaire(c - p);

        Collision { 
            point: p,
            point1: p.projeter_dans(p1), 
            point2: p.projeter_dans(p2), 
            temps, 
            normale,
        }
    }
}

impl Intersection<Cercle, Segment> for Collision {
    fn intersection(
           forme1: &Cercle, forme2: &Segment,
           position1: Position, position2: Position,
        ) -> bool {
        let a = forme2.debut.projeter_hors(position2);
        let b = forme2.fin.projeter_hors(position2);
        let ab = Vecteur::unitaire(b - a);
        let n = (b - a).norme();
        let ac = forme1.centre.projeter_hors(position1) - a;
        let p = ac.produit_scalaire(ab);
        let d = (ac - p * ac).norme();
        (d <= forme1.rayon && p >= 0. && p <= n)
        || Collision::intersection(forme1, &Cercle { centre: forme2.debut, rayon: 0. }, position1, position2)
        || Collision::intersection(forme1, &Cercle { centre: forme2.fin, rayon: 0. }, position1, position2)
    }
    
    fn collision(f1: &Cercle, f2: &Segment, p1: Position, p2: Position,
            v1: Vitesse, v2: Vitesse, temps: Scalaire
        ) -> Self {
        //TODO: Implémenter les cas extrêmes (coins)
        Self::collision(f1, &Droite { debut: f2.debut, fin: f2.fin }, p1, p2, v1, v2, temps)
    }
}
