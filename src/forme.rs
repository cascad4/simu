use crate::math::intersection::{Cercle, Droite, Intersection, Collision};

#[derive(Debug, Clone)]
pub enum Forme {
    Cercle(Cercle),
    Droite(Droite),
}

impl Intersection<Cercle, Forme> for Collision {
    fn intersection(
            forme1: &Cercle, forme2: &Forme,
            position1: crate::math::coordonnees::Position, position2: crate::math::coordonnees::Position,
        ) -> bool {
        match forme2 {
            Forme::Cercle(cercle) => Collision::intersection(forme1, cercle, position1, position2),
            Forme::Droite(droite) => Collision::intersection(forme1, droite, position1, position2),
        }
    }

    fn collision(f1: &Cercle, f2: &Forme, p1: crate::math::coordonnees::Position, p2: crate::math::coordonnees::Position,
            v1: crate::math::coordonnees::Vitesse, v2: crate::math::coordonnees::Vitesse, temps: crate::math::coordonnees::Scalaire
        ) -> Self {
        match f2 {
            Forme::Cercle(cercle) => Collision::collision(f1, cercle, p1, p2, v1, v2, temps),
            Forme::Droite(droite) => Collision::collision(f1, droite, p1, p2, v1, v2, temps),
        }
    }
}

impl Intersection<Droite, Forme> for Collision {
    fn intersection(
            forme1: &Droite, forme2: &Forme,
            position1: crate::math::coordonnees::Position, position2: crate::math::coordonnees::Position,
        ) -> bool {
        match forme2 {
            Forme::Cercle(cercle) => Collision::intersection(cercle, forme1, position2, position1),
            Forme::Droite(_droite) => false,
        }
    }

    fn collision(f1: &Droite, f2: &Forme, p1: crate::math::coordonnees::Position, p2: crate::math::coordonnees::Position,
            v1: crate::math::coordonnees::Vitesse, v2: crate::math::coordonnees::Vitesse, temps: crate::math::coordonnees::Scalaire
        ) -> Self {
        match f2 {
            Forme::Cercle(cercle) => Collision::collision(cercle, f1, p2, p1, v2, v1, temps),
            Forme::Droite(_droite) => Collision::NUL,
        }
    }
}

impl Intersection<Forme, Forme> for Collision {
    fn intersection(
            forme1: &Forme, forme2: &Forme,
            position1: crate::math::coordonnees::Position, position2: crate::math::coordonnees::Position,
        ) -> bool {
        match forme1 {
            Forme::Cercle(forme1) => Collision::intersection(forme1, forme2, position1, position2),
            Forme::Droite(forme1) => Collision::intersection(forme1, forme2, position1, position2),
        }
    }

    fn collision(f1: &Forme, f2: &Forme, p1: crate::math::coordonnees::Position, p2: crate::math::coordonnees::Position,
            v1: crate::math::coordonnees::Vitesse, v2: crate::math::coordonnees::Vitesse, temps: crate::math::coordonnees::Scalaire
        ) -> Self {
        match f1 {
            Forme::Cercle(cercle) => Collision::collision(cercle, f2, p1, p2, v1, v2, temps),
            Forme::Droite(droite) => Collision::collision(droite, f2, p1, p2, v1, v2, temps),
        }
    }
}
