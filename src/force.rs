use std::f32::consts::PI;

use crate::{math::{coordonnees::{Scalaire, Vecteur, Point, Vitesse}, intersection::Collision}, objet::Objet};

const G: Scalaire = 9.81;

#[derive(Debug, Clone)]
pub struct Force {
    pub debut: Scalaire,
    pub fin: Scalaire,
    pub vecteur: Vecteur,
    pub point: Point,
    pub source: usize,
}

impl Force {
    pub fn poids(m: Scalaire, debut: Scalaire, pas: Scalaire) -> Force {
        Force {
            debut,
            fin: debut + pas * 0.99,
            vecteur: Vecteur::nouveau(0., -m * G),
            point: Point::NUL,
            source: usize::MAX,
        }
    }

    pub fn trainee(f: Scalaire, v: Vitesse, debut: Scalaire, pas: Scalaire) -> Force {
        Force {
            debut,
            fin: debut + pas * 0.99,
            vecteur: -f * v.norme() * v.origine(),
            point: Point::NUL,
            source: usize::MAX
        }
    }

    pub fn collision(col: &Collision, obj1: &Objet, obj2: &Objet, debut: Scalaire, pas: Scalaire) -> Force {
        let e = 0.3;
        let f = 0.1;
        let k = 10000.;
        
        let normale = col.normale;
        let tangente = Vecteur::orthogonal(normale);
        let m1 = obj1.masse;
        let m2 = obj2.masse;
        let v1 = obj1.vitesse.origine() + obj1.vitesse.a * col.point1;
        let v2 = obj2.vitesse.origine() + obj2.vitesse.a * col.point2;
        let vni = normale.produit_scalaire(v2 - v1).abs();
        let vt = tangente.produit_scalaire(v2 - v1);
        let vnf = e * vni * m2 / (m1 + m2);

        let w0 = (k/m1).sqrt();
        let dt = 3.14 / w0;
        let n = m1 * (vnf + vni) / dt;

        let t;
        
        if vt > 0. {
            t = f * n;
        } else {
            t = - f * n;
        }
        dbg!(vni, vnf);

        Force {
            debut: debut + col.temps,
            fin: debut + col.temps + dt,
            vecteur: PI/22. * n * normale + PI/22.* t * tangente,
            point: col.point1,
            source: obj2.id as usize,
        }
    }

    pub fn resultante(&self) -> Vecteur {
        if self.point.norme() < 1e-6 {
            return self.vecteur;
        }
        self.vecteur.produit_scalaire(self.point) * self.point
    }

    pub fn moment(&self) -> Scalaire {
        if self.point.norme() < 1e-6 {
            return 0.;
        }
        self.vecteur.produit_scalaire(Vecteur::orthonorme(-self.point))
    }
}
