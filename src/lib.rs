pub mod force;
pub mod forme;
pub mod math;
pub mod objet;
pub mod simulation;

pub fn assert_flottant(x: f32, y: f32, d: f32) {
    if (x - y).abs() > d {
        panic!("{} != {} (+/-) {}", x, y, d)
    }
}

#[cfg(test)]
mod tests_geometrie {
    use std::f32::consts::PI;

    use crate::assert_flottant;
    use crate::math::coordonnees::{Vecteur, Repere, Point};

    #[test]
    fn test_produit_scalaire() {
        let a = Vecteur::nouveau(1., 0.);
        let b = Vecteur::nouveau(0., 1.);
        assert_flottant(a.produit_scalaire(b), 0., 1e-5);
        assert_flottant(a.produit_scalaire(0.*a), 0., 1e-5);
        assert_flottant(a.produit_scalaire(-a), -1., 1e-5);
        assert_flottant(a.produit_scalaire(10.*a), 10., 1e-5);
    }

    #[test]
    fn test_orthogonal() {
        let a = Vecteur::nouveau(1., 0.);
        let b = Vecteur::nouveau(0., 1.);
        let c = Vecteur::orthogonal(a);
        let d = Vecteur::orthogonal(b);
        assert_flottant(b.distance(c), 0., 1e-5);
        assert_flottant(a.distance(-d), 0., 1e-5);
    }

    #[test]
    fn test_trigonometrie() {
        let a = Vecteur::nouveau(1., 1.);
        assert_flottant(a.argument(), PI/4., 1e-5);
        assert_flottant(a.norme(), 2f32.powf(0.5), 1e-5);
        assert_flottant(a.distance(Vecteur::trigo(2f32.powf(0.5), PI/4.)), 0., 1e-5);
        assert_flottant(Vecteur::trigo(1., 2.).argument(), 2., 1e-5);
        assert_flottant(Vecteur::trigo(1., 2.).norme(), 1., 1e-5);
        assert_flottant(Vecteur::trigo(5., -3.).argument(), -3. + 2.*PI, 1e-5);
        assert_flottant(Vecteur::trigo(5., -3.).norme(), 5., 1e-5);
    }

    #[test]
    fn test_projection() {
        let r1 = Repere::nouveau(0., 0., 0.);
        let r2 = Repere::nouveau(1., 1., PI/2.);
        let r3 = Repere::nouveau(1., -1., PI/4.);
        let p0 = Point::NUL;
        let p1 = Point::nouveau(1., 1.);
        let p2 = Point::nouveau(1., -1.);
        let p3 = Point::trigo(1., PI/3.);
        assert_flottant(p1.projeter_hors(r1).distance(p1), 0., 1e-5);
        assert_flottant(p2.projeter_hors(r1).distance(p2), 0., 1e-5);
        assert_flottant(p3.projeter_hors(r1).distance(p3), 0., 1e-5);
        assert_flottant(p1.projeter_dans(r1).distance(p1), 0., 1e-5);
        assert_flottant(p2.projeter_dans(r1).distance(p2), 0., 1e-5);
        assert_flottant(p3.projeter_dans(r1).distance(p3), 0., 1e-5);
       
        assert_flottant(p0.projeter_hors(r2).distance(p1), 0., 1e-5);
        assert_flottant(p0.projeter_hors(r3).distance(p2), 0., 1e-5);
        assert_flottant(p1.projeter_dans(r2).norme(), 0., 1e-5);
        assert_flottant(p2.projeter_dans(r3).norme(), 0., 1e-5);
        
        assert_flottant((p1 + p3).projeter_dans(r2).distance(Point::trigo(1., -PI/6.)), 0., 1e-5);
        assert_flottant((p2 + p3).projeter_dans(r3).distance(Point::trigo(1., PI/12.)), 0., 1e-5);
        assert_flottant(p3.projeter_hors(r2).distance(p1 + Point::trigo(1., 5.*PI/6.)), 0., 1e-5);
        assert_flottant(p3.projeter_hors(r3).distance(p2 + Point::trigo(1., 7.*PI/12.)), 0., 1e-5);
    }
}

#[cfg(test)]
mod tests_collision {
    use crate::{assert_flottant, math::{intersection::{Cercle, Collision, Intersection, Droite}, coordonnees::{Vecteur, Position}}};

    #[test]
    fn test_collision_cercle_cercle() {
        let c1 = Cercle { centre: Vecteur::NUL, rayon: 5. };
        let c2 = Cercle { centre: Vecteur::trigo(9., 0.), rayon: 5. };
        let c3 = Cercle { centre: Vecteur::trigo(6.5, 0.), rayon: 1.};
        let p1 = Position::nouveau(0., 0., 0.);
        let p2 = Position::nouveau(2., 4., 3.);
        assert!(Collision::intersection(&c1, &c1, p1, p1));
        assert!(Collision::intersection(&c1, &c1, p1, p2));
        assert!(Collision::intersection(&c1, &c2, p1, p1));
        assert!(!Collision::intersection(&c1, &c3, p1, p1));
        assert!(Collision::intersection(&c2, &c3, p1, p1));
    }

    #[test]
    fn test_collision_cercle_droite() {
        let c1 = Cercle { centre: Vecteur::NUL, rayon: 5.};
        let d1 = Droite { debut: Vecteur::NUL, fin: Vecteur { x: 1., y: 0. } };
        let p1 = Position::nouveau(0., 0., 0.);
        let p2 = Position::nouveau(1., 2., 3.);
        let p3 = Position::nouveau(0., 6., -0.1);
        assert!(Collision::intersection(&c1, &d1, p1, p1));
        assert!(Collision::intersection(&c1, &d1, p1, p2));
        assert!(!Collision::intersection(&c1, &d1, p1, p3));
    }
}
